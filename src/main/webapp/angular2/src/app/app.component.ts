import {Component, OnInit} from "@angular/core";
import {FormControl, FormGroup} from "@angular/forms";
import {Http, Request, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs";
import { map, catchError} from 'rxjs/operators';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    constructor(private http: Http) { }

    private baseURL: string = 'http://localhost:8080';
    public submitted: boolean;
    bikeboxsearch: FormGroup;
    bikeboxes: Bikebox[];
    request:ReserveBikeboxRequest;
    currentCheckOutVal: string;
    currentCheckInVal: string;

    ngOnInit() {
        this.bikeboxsearch = new FormGroup({
            checkout: new FormControl(''),
            checkin: new FormControl('')
        });

        /*removed from past labs
        this.bikeboxes = BIKEBOXES;*/

        const bikeboxsearchValueChanges$ = this.bikeboxsearch.valueChanges;

        bikeboxsearchValueChanges$.subscribe(valChange => {
            this.currentCheckOutVal = valChange.checkout;
            this.currentCheckInVal = valChange.checkin;
        });
    }


    onSubmit({value, valid}: { value: Bikeboxsearch, valid: boolean }) {
        this.getAll()
            .subscribe(
            bikeboxes => this.bikeboxes = bikeboxes,
            err => {
                console.log(err)
            }
            );
    }

    reserveBikeBox(bikeBoxId: string) {
        this.request = new ReserveBikeboxRequest(bikeBoxId,this.currentCheckOutVal, this.currentCheckInVal);
        this.createReservation(this.request);
    }

    getAll(): Observable<Bikebox[]> {
        return this.http
            .get(this.baseURL + '/bikebox/reservation/v1?checkout='+ this.currentCheckOutVal + '&checkin=' + this.currentCheckOutVal)
            .pipe(map(this.mapBikebox));
    }

    createReservation(body: ReserveBikeboxRequest){
        let bodyString = JSON.stringify(body);
        let headers = new Headers({'Content-Type': 'application/json'});
        let option = new RequestOptions({headers:headers});
        
        this.http.post(this.baseURL + '/bikebox/reservation/v1', body, option)
        .subscribe(res => {console.log(res)});
    }
    
    mapBikebox(response: Response): Bikebox[] {
        return response.json().content;

    }

}

export interface Bikeboxsearch {
    checkout: string;
    checkin: string;
}

export interface Bikebox {
    id: string;
    bikeBoxNumber: string;
    links: string;
}

export class ReserveBikeboxRequest {
    bikeBoxId: string;
    checkout: string;
    checkin: string;

    constructor(
        bikeBoxId: string,
        checkout: string,
        checkin: string
    ) {
        this.bikeBoxId = bikeBoxId;
        this.checkout = checkout;
        this.checkin = checkin;
    }


}


/* Removes the hard coded data from past steps
var BIKEBOXES:Bikebox[] = [
  {
      "id": "37489234327",
      "bikeBoxNumber": "1",
      "links": ""
  },
  {
    "id": "84329874798",
    "bikeBoxNumber": "2",
    "links": ""
  }
];
*/


