package org.brooklyntri.bikebox.repository;


import java.util.Optional;

import org.brooklyntri.bikebox.entity.BikeBoxEntity;
import org.springframework.data.repository.CrudRepository;

public interface BikeBoxRepository extends CrudRepository<BikeBoxEntity, Long> {

	Optional<BikeBoxEntity> findById(Long id);
	

}// interface
