package org.brooklyntri.bikebox.repository;

import org.brooklyntri.bikebox.entity.BikeBoxEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;



public interface PageableBikeBoxRepository extends PagingAndSortingRepository<BikeBoxEntity, Long> {
	
	Page<BikeBoxEntity> findById(Long id, Pageable page);

}
