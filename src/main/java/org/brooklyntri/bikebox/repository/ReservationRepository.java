package org.brooklyntri.bikebox.repository;

import org.brooklyntri.bikebox.entity.ReservationEntity;
import org.springframework.data.repository.CrudRepository;

public interface ReservationRepository extends CrudRepository<ReservationEntity, Long> {

}
