package org.brooklyntri.bikebox;

import org.brooklyntri.bikebox.entity.BikeBoxEntity;
import org.brooklyntri.bikebox.repository.BikeBoxRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class H2Bootstrap implements CommandLineRunner {

	@Autowired
	BikeBoxRepository bikeBoxRepository;

	@Override
	public void run(String... args) throws Exception {

		System.out.println("Bootstrapping data: ");

		// TODO Auto-generated method stub

		bikeBoxRepository.save(new BikeBoxEntity(1));
		bikeBoxRepository.save(new BikeBoxEntity(2));

		Iterable<BikeBoxEntity> itr = bikeBoxRepository.findAll();

		System.out.println("Printing out data: ");
		for (BikeBoxEntity bikebox : itr) {
			System.out.println(bikebox.getBikeBoxNumber());
		}

	}

}
