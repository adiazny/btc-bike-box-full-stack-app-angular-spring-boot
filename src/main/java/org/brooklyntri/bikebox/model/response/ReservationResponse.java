package org.brooklyntri.bikebox.model.response;

import java.time.LocalDate;

public class ReservationResponse {

	private Long id;
	private LocalDate checkout;
	private LocalDate checkin;

	public ReservationResponse() {
		super();
	}

	public ReservationResponse(Long id, LocalDate checkout, LocalDate checkin) {
		super();
		this.id = id;
		this.checkout = checkout;
		this.checkin = checkin;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getCheckout() {
		return checkout;
	}

	public void setCheckout(LocalDate checkout) {
		this.checkout = checkout;
	}

	public LocalDate getCheckin() {
		return checkin;
	}

	public void setCheckin(LocalDate checkin) {
		this.checkin = checkin;
	}

}
