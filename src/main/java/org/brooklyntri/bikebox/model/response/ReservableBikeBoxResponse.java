package org.brooklyntri.bikebox.model.response;

import org.brooklyntri.bikebox.model.Links;

public class ReservableBikeBoxResponse {

	private Long id;
	private Integer bikeBoxNumber;
	private Links links;
	
	
	public ReservableBikeBoxResponse() {
		super();
	}//default constructor

	public ReservableBikeBoxResponse(Integer bikeBoxNumber) {
		super();
		this.bikeBoxNumber = bikeBoxNumber;
	}//constructor #1
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getBikeBoxNumber() {
		return bikeBoxNumber;
	}
	public void setBikeBoxNumber(Integer bikeBoxNumber) {
		this.bikeBoxNumber = bikeBoxNumber;
	}
	public Links getLinks() {
		return links;
	}
	public void setLinks(Links links) {
		this.links = links;
	}	
	
}//class
