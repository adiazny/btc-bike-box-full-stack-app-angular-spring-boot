package org.brooklyntri.bikebox.model.request;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

public class ReservationRequest {

	private Long id;
	private Long bikeBoxId;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate checkout;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate checkin;

	public ReservationRequest() {
		super();
	}

	public ReservationRequest(Long bikeBoxId, LocalDate checkout, LocalDate checkin) {
		super();
		this.bikeBoxId = bikeBoxId;
		this.checkout = checkout;
		this.checkin = checkin;
	}

	public Long getBikeBoxId() {
		return bikeBoxId;
	}

	public void setBikeBoxId(Long bikeBoxId) {
		this.bikeBoxId = bikeBoxId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getCheckout() {
		return checkout;
	}

	public void setCheckout(LocalDate checkout) {
		this.checkout = checkout;
	}

	public LocalDate getCheckin() {
		return checkin;
	}

	public void setCheckin(LocalDate checkin) {
		this.checkin = checkin;
	}

}// class
