package org.brooklyntri.bikebox.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Reservation")
public class ReservationEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	private LocalDate checkout;

	@NotNull
	private LocalDate checkin;

	@ManyToOne
	private BikeBoxEntity bikeBoxEntity;

	public ReservationEntity() {
		super();
	}

	public ReservationEntity(@NotNull LocalDate checkout, @NotNull LocalDate checkin) {
		super();
		this.checkout = checkout;
		this.checkin = checkin;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getCheckout() {
		return checkout;
	}

	public void setCheckout(LocalDate checkout) {
		this.checkout = checkout;
	}

	public LocalDate getCheckin() {
		return checkin;
	}

	public void setCheckin(LocalDate checkin) {
		this.checkin = checkin;
	}

	public BikeBoxEntity getBikeBoxEntity() {
		return bikeBoxEntity;
	}

	public void setBikeBoxEntity(BikeBoxEntity bikeBoxEntity) {
		this.bikeBoxEntity = bikeBoxEntity;
	}

}// class
