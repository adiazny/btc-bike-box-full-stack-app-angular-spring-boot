package org.brooklyntri.bikebox.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "BikeBox")
public class BikeBoxEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	private Integer bikeBoxNumber;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	private List<ReservationEntity> reservationEntityList;

	public BikeBoxEntity() {
		super();
	}

	public BikeBoxEntity(@NotNull Integer bikeBoxNumber) {
		super();
		this.bikeBoxNumber = bikeBoxNumber;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBikeBoxNumber() {
		return bikeBoxNumber;
	}

	public void setBikeBoxNumber(Integer bikeBoxNumber) {
		this.bikeBoxNumber = bikeBoxNumber;
	}

	public List<ReservationEntity> getReservationEntityList() {
		return reservationEntityList;
	}

	public void setReservationEntityList(List<ReservationEntity> reservationEntityList) {
		this.reservationEntityList = reservationEntityList;
	}
	
	public void addReservationEntity(ReservationEntity reservationEntity){
		if(null == reservationEntityList)
			reservationEntityList = new ArrayList<>();
		reservationEntityList.add(reservationEntity);
	}
	

}//class
