package org.brooklyntri.bikebox.config;

import java.util.HashSet;
import java.util.Set;

import org.brooklyntri.converter.BikeBoxEntityToReservableBikeBoxResponseConverter;
import org.brooklyntri.converter.ReservationEntityToReservationResponseConverter;
import org.brooklyntri.converter.ReservationRequestToReservationEntityConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

@Configuration
public class ConversionConfig {

	private Set<Converter> getConverters() {
		Set<Converter> converters = new HashSet<Converter>();
		converters.add(new BikeBoxEntityToReservableBikeBoxResponseConverter());
		converters.add(new ReservationRequestToReservationEntityConverter());
		converters.add(new ReservationEntityToReservationResponseConverter());

		return converters;
	}

	@Bean
	public ConversionService conversionService() {
		ConversionServiceFactoryBean bean = new ConversionServiceFactoryBean();
		bean.setConverters(getConverters());
		bean.afterPropertiesSet();

		return bean.getObject();
	}
	
	@Bean
	public BikeBoxEntityToReservableBikeBoxResponseConverter bikeBoxEntityToReservableBikeBoxResponseConverter() {
		return new BikeBoxEntityToReservableBikeBoxResponseConverter();
	}
	
}// class
