package org.brooklyntri.bikebox.config;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableJpaRepositories("org.brooklyntri.bikebox.repository")
@EnableTransactionManagement
public class DatabaseConfig {
	
	

}//class
