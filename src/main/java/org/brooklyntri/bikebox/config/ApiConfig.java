package org.brooklyntri.bikebox.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Configuration//spring will use this annotation for configuration
public class ApiConfig {
	
	/*
	 * ObjectMapper will define how JSON strings in the request body are deserialized from 
	 * requests in POJOs or Plain Old Java Objects, which we use to model our data.
	 */

	@Bean //creating an object of type ObjectMapper with a name of objectMapper
	public ObjectMapper objectMapper() {
		ObjectMapper objectMapper= new ObjectMapper();
		objectMapper.registerModule(new JavaTimeModule());
		
		return objectMapper;
	}//objectMapper()
	
	/*
	 * ObjectWriter will define how we serialize our Java objects into a JSON string in 
	 * the response body.
	 * 
	 * Takes in a ObjectMapper as a parameter.
	 * 
	 * use the ObjectMapper to create a Default PrettyPrinter, 
	 * which will output a JSON in Human Readable Format.
	 */	
	
	@Bean
	public ObjectWriter objectWriter(ObjectMapper objectMapper) {
		return objectMapper.writerWithDefaultPrettyPrinter();
	}//objectWriter()
	
	
	
	
	

}//class
