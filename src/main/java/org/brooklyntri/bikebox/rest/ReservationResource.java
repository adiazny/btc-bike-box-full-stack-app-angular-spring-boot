package org.brooklyntri.bikebox.rest;

import java.time.LocalDate;
import java.util.Optional;

import org.brooklyntri.bikebox.entity.BikeBoxEntity;
import org.brooklyntri.bikebox.entity.ReservationEntity;
import org.brooklyntri.bikebox.model.request.ReservationRequest;
import org.brooklyntri.bikebox.model.response.ReservableBikeBoxResponse;
import org.brooklyntri.bikebox.model.response.ReservationResponse;
import org.brooklyntri.bikebox.repository.BikeBoxRepository;
import org.brooklyntri.bikebox.repository.PageableBikeBoxRepository;
import org.brooklyntri.bikebox.repository.ReservationRepository;
import org.brooklyntri.converter.BikeBoxEntityToReservableBikeBoxResponseConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(ResourceConstants.BIKEBOX_RESERVATION_V1)
@CrossOrigin
public class ReservationResource {
	
	@Autowired
	PageableBikeBoxRepository pageableBikeBoxRepository;
	
	@Autowired
	BikeBoxRepository bikeBoxRepository;
	
	@Autowired
	ReservationRepository reservationRepository;
	
	@Autowired
	ConversionService conversionService;
	
	@Autowired
	private BikeBoxEntityToReservableBikeBoxResponseConverter converter;
	
	@RequestMapping(path="", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public Page<ReservableBikeBoxResponse> getAvailableBikeBoxes(
			@RequestParam(value = "checkout")
			@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
			LocalDate checkout, 
			@RequestParam(value = "checkin")
			@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
			LocalDate checkin, Pageable pageable){
		
		Page<BikeBoxEntity> bikeBoxEntityList = pageableBikeBoxRepository.findAll(pageable);
		
		//return new ResponseEntity<>(new ReservableBikeBoxResponse(), HttpStatus.OK);
		return bikeBoxEntityList.map(converter::convert);
		
	}//getAvailableBikeBoxes()
	
	@RequestMapping(path= "/{bikeBoxId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<BikeBoxEntity> getBikeBoxById(
			@PathVariable
			Long bikeBoxId){
		
		Optional<BikeBoxEntity> bikeBoxEntity = bikeBoxRepository.findById(bikeBoxId);
		if (bikeBoxEntity.isPresent()) {
			return new ResponseEntity<BikeBoxEntity>(bikeBoxEntity.get(), HttpStatus.OK);
		}
		return null;
	}//getBikeBoxByID()

	@RequestMapping(path="", method=RequestMethod.POST, produces= MediaType.APPLICATION_JSON_UTF8_VALUE,
			consumes= MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ReservationResponse> createReservation(
			@RequestBody
			ReservationRequest reservationRequest){
		
		ReservationEntity reservationEntity = conversionService.convert(reservationRequest, ReservationEntity.class);
		reservationRepository.save(reservationEntity);
		
		Optional<BikeBoxEntity> optionalBikeBoxEntity = bikeBoxRepository.findById(reservationRequest.getBikeBoxId());
		if(optionalBikeBoxEntity.isPresent()){
			BikeBoxEntity bikeBikeEntity = optionalBikeBoxEntity.get();
			bikeBikeEntity.addReservationEntity(reservationEntity);
			bikeBoxRepository.save(bikeBikeEntity);
			reservationEntity.setBikeBoxEntity(bikeBikeEntity);
		}//if
		
		ReservationResponse reservationResponse = conversionService.convert(reservationEntity, ReservationResponse.class);
		
		return new ResponseEntity<>(reservationResponse, HttpStatus.CREATED);
	}//createReservation()
	
	@RequestMapping(path="", method=RequestMethod.PUT, produces= MediaType.APPLICATION_JSON_UTF8_VALUE,
			consumes= MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ReservableBikeBoxResponse> updateReservation(
			@RequestBody
			ReservationRequest reservationRequest){
		
		return new ResponseEntity<>(new ReservableBikeBoxResponse(),HttpStatus.OK);
	}//updateReservation()
	
	@RequestMapping(path="/{reservationId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteReservation(
			@PathVariable
			long reservationId){
		
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}//deleteReservation()
	
	
}//class
