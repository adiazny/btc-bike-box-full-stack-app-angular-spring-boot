package org.brooklyntri.bikebox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

//@SpringBootApplication enables component scanning and auto-configuration. 
//(A combo of 3 annotations: @Configuration, @EnableAutoConfiguration and @ComponentScan
@SpringBootApplication

//@EnableAutoConfiguration will intelligently configure beans that you are likely to need in your Spring application context.
@EnableAutoConfiguration
//@ComponentScan will enable automatic scanning for configuration classes to wire up in your Spring application context. 
@ComponentScan
public class BtcBikeBoxFullStackAppAngularSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(BtcBikeBoxFullStackAppAngularSpringBootApplication.class, args);
				
	}//main()
}
