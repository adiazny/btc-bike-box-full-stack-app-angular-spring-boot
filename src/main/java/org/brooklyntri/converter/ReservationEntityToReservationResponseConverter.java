package org.brooklyntri.converter;


import org.brooklyntri.bikebox.entity.ReservationEntity;
import org.brooklyntri.bikebox.model.response.ReservationResponse;
import org.springframework.core.convert.converter.Converter;

public class ReservationEntityToReservationResponseConverter implements Converter<ReservationEntity, ReservationResponse> {

	@Override
	public ReservationResponse convert(ReservationEntity source) {
		// TODO Auto-generated method stub

		ReservationResponse reservationResponse = new ReservationResponse();
		reservationResponse.setCheckout(source.getCheckout());
		reservationResponse.setCheckin(source.getCheckin());
		
		if (null != source.getBikeBoxEntity())
        reservationResponse.setId(source.getBikeBoxEntity().getId());
		
		return reservationResponse;
	}

}
