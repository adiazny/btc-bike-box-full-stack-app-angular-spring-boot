package org.brooklyntri.converter;

import org.brooklyntri.bikebox.entity.BikeBoxEntity;
import org.brooklyntri.bikebox.model.Links;
import org.brooklyntri.bikebox.model.Self;
import org.brooklyntri.bikebox.model.response.ReservableBikeBoxResponse;
import org.brooklyntri.bikebox.rest.ResourceConstants;
import org.springframework.core.convert.converter.Converter;


public class BikeBoxEntityToReservableBikeBoxResponseConverter
		implements Converter<BikeBoxEntity, ReservableBikeBoxResponse> {

	@Override
	public ReservableBikeBoxResponse convert(BikeBoxEntity source) {
		// TODO Auto-generated method stub

		ReservableBikeBoxResponse reservationResponse = new ReservableBikeBoxResponse();
		if (null != source.getId())
			reservationResponse.setId(source.getId());
		reservationResponse.setBikeBoxNumber(source.getBikeBoxNumber());

		Links links = new Links();
		Self self = new Self();
		self.setRef(ResourceConstants.BIKEBOX_RESERVATION_V1 + "/" + source.getId());
		links.setSelf(self);

		reservationResponse.setLinks(links);

		return reservationResponse;
	}

}
