package org.brooklyntri.converter;

import org.brooklyntri.bikebox.entity.ReservationEntity;
import org.brooklyntri.bikebox.model.request.ReservationRequest;
import org.springframework.core.convert.converter.Converter;

public class ReservationRequestToReservationEntityConverter
		implements Converter<ReservationRequest, ReservationEntity> {

	@Override
	public ReservationEntity convert(ReservationRequest source) {
		// TODO Auto-generated method stub

		ReservationEntity reservationEntity = new ReservationEntity();
		reservationEntity.setCheckout(source.getCheckout());
		reservationEntity.setCheckin(source.getCheckin());

		if (null != source.getId())
			reservationEntity.setId(source.getId());

		return reservationEntity;
	}

}
